﻿using System;

namespace Translator.Core
{
    /// <summary>
    /// Attribute specifies which language code corresponds to the plug-in
    /// </summary>
    [Serializable]
    [AttributeUsage(AttributeTargets.Class)]
    public class TranslationLanguageAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="languageCode">Language code</param>
        public TranslationLanguageAttribute(string languageCode)
        {
            _languageCode = languageCode;
        }

        public override string ToString()
        {
            return _languageCode;
        }

        protected bool Equals(TranslationLanguageAttribute other)
        {
            return string.Equals(_languageCode, other._languageCode);
        }

        public override bool Equals(object obj)
        {
            // ReSharper disable once UseNullPropagation
            if (obj == null)
            {
                return false;
            }

            var attr = obj as TranslationLanguageAttribute;
            if (attr == null)
            {
                return false;
            }

            var res = Equals(attr);
            return res;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (_languageCode != null ? _languageCode.GetHashCode() : 0);
            }
        }

        private readonly string _languageCode;
    }
}