﻿using System;
using System.Collections.Generic;

namespace Translator.Plugins.Core
{
    public interface IPluginTranslatorMap<TKey>: IDisposable,
        IEnumerable<KeyValuePair<TKey, IPluginTranslatorsContainer>>
    {
        IPluginTranslatorsContainer this[TKey key] { get; }
        IPluginTranslatorMap<TKey> Add(TKey key, IPluginTranslatorsContainer container);
        IPluginTranslatorMap<TKey> Remove(TKey key);
        bool Contains(TKey key);
    }
}
