﻿using System;
using Translator.Core;
using Translator.Plugins.Core;

namespace Translator
{
    /// <summary>
    /// Wrap over the plug-in, loaded into a separate application domain.
    /// </summary>
    public class AppDomainTranslatorsContainer : IPluginTranslatorsContainer
    {
        public AppDomainTranslatorsContainer(string cachePath, string shadowCopyDirectories,
            Func<AppDomain, IPluginTranslatorsContainer> getInnerContainer)
        {
            // This creates a ShadowCopy of the DLL's in the ShadowCopyDirectories
            var setup = new AppDomainSetup
            {
                CachePath = cachePath,
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = shadowCopyDirectories
            };

            var uuid = Guid.NewGuid();
            var appDomainName = string.Concat("Host_AppDomain_", uuid);
            try
            {
                _appDomain = AppDomain.CreateDomain(appDomainName, AppDomain.CurrentDomain.Evidence,
                setup);

                _innerContainer = getInnerContainer(_appDomain);
            }
            catch (PluginException ex)
            {
                Dispose();
                InnerException = ex;
            }
        }

        public void Dispose()
        {
            if (_innerContainer != null)
            {
                _innerContainer.Dispose();
            }
            if (_appDomain != null)
            {
                AppDomain.Unload(_appDomain);
            }
        }

        public PluginException InnerException { get; private set; }

        public string Translate(string inputText, Attribute targetAttribute)
        {
            return InnerException == null
                ? _innerContainer.Translate(inputText, targetAttribute)
                : InnerException.Message;
        }

        private readonly AppDomain _appDomain;
        private readonly IPluginTranslatorsContainer _innerContainer;
    }
}