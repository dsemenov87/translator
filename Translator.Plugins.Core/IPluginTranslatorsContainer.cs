﻿using System;

namespace Translator.Plugins.Core
{
    /// <summary>
    /// Container to wrap translating plug-in
    /// </summary>
    public interface IPluginTranslatorsContainer : IDisposable
    {
        string Translate(string inputText, Attribute targetAttribute);
        PluginException InnerException { get; }
    }
}