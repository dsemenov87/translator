﻿using System.Linq;
using Translator.Core;
using Translator.Plugins.Core;

namespace Translator
{
    public static class Translator
    {
        /// <summary>
        /// The main translation method
        /// </summary>
        /// <typeparam name="TPluginKey"></typeparam>
        /// <param name="inputText">Text to translate</param>
        /// <param name="targetAttribute">Attribute defines the 
        /// target language</param>
        /// <param name="pluginMap">the map of available plug-ins</param>
        /// <returns></returns>
        public static string Translate<TPluginKey>(string inputText,
            TranslationLanguageAttribute targetAttribute,
            IPluginTranslatorMap<TPluginKey> pluginMap)
        {
            try
            {
                var query = from pair in pluginMap
                            let container = pair.Value
                            let translated = container.Translate(inputText, targetAttribute)
                            select translated;

                return query.FirstOrDefault();
            }
            catch (TranslatorException ex)
            {
                var message = string.Format("When translating error occurred: {0}", ex.Message);
                return message;
            }
        }
    }

}
