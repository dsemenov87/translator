﻿using System;
using System.Linq;
using System.Reflection;
using Translator.Core;

namespace Translator
{
    public static class TranslatorExtensions
    {
        public static bool HasAttribute(this ITranslator translator, Attribute target)
        {
            var typ = translator.GetType();
            var attrs = typ.GetCustomAttributes();
            var res = attrs.Any(a => a.Equals(target));
            return res;
        }
    }

}
