﻿using System;
using System.IO;
using System.Reflection;
using Translator.Core;
using Translator.Plugins.Core;

namespace Translator.App
{
    class Program
    {
        private static readonly string _cachePath;
        private static readonly string _shadowCopyDirectories;

        static Program()
        {
            var appBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            _cachePath = Path.Combine(appBase, Resource1.ShadowCopyCachePath);
            _shadowCopyDirectories = Path.Combine(appBase, Resource1.ShadowCopyDirectoriesPath);

            Directory.CreateDirectory(_cachePath);
            Directory.CreateDirectory(_shadowCopyDirectories);
        }

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                var key = args[0].TrimStart();
                if (key != "--help")
                {
                    Console.WriteLine(Resource1.UnknownKeyMessage, key);
                    return;
                }

                Console.WriteLine(Resource1.HelpMessage);
                return;
            }

            using (var translatorPlugins = new PluginTranslatorMap<string>())
            {
                while (true)
                {
                    CommandEnterIteration(translatorPlugins);
                }
            }
        }

        private static void LoadPlugin(string inputText, PluginTranslatorMap<string> translatorPlugins)
        {
            var pluginPath = inputText.Substring(3);
            var appDomainTranslatorsContainer
                = new AppDomainTranslatorsContainer(_cachePath, _shadowCopyDirectories, appDomain =>
                    CreateInnerContainer(appDomain, pluginPath));

            if (translatorPlugins.Contains(pluginPath))
            {
                Console.WriteLine(Resource1.PluginIsAlreadyLoadedMessage);
            }
            else
            {
                var innerException = appDomainTranslatorsContainer.InnerException;
                if (innerException == null)
                {
                    translatorPlugins.Add(pluginPath, appDomainTranslatorsContainer);
                    Console.WriteLine(string.Concat(Resource1.PluginSuccessfullyLoaded, "\n"));
                }
                else
                {
                    var message = string.Concat(Resource1.PluginNotLoaded, "\n",
                        innerException.Message);
                    Console.WriteLine(message);
                }
            }
        }

        private static void UnloadPlugin(string inputText, PluginTranslatorMap<string> translatorPlugins)
        {
            var pluginPath = inputText.Substring(3).Trim();
            if (translatorPlugins.Contains(pluginPath))
            {
                translatorPlugins.Remove(pluginPath);
                Console.WriteLine(string.Concat(Resource1.PluginSuccessfullyUnloaded, "\n"));
            }
            else
            {
                Console.WriteLine(string.Concat(Resource1.PluginNotFound, "\n"));
            }
        }

        private static void Traslate(string rawInputText, PluginTranslatorMap<string> translatorPlugins)
        {
            var languageCode = rawInputText.Substring(3).Trim();
            var targetAttribute = new TranslationLanguageAttribute(languageCode);
            Console.WriteLine(string.Concat("\n", Resource1.EnterTextToTranslateMessage));
            var inputText = Console.ReadLine().Trim();
            var translated = Translator.Translate(inputText, targetAttribute, translatorPlugins);
            if (translated == null)
            {
                Console.WriteLine(string.Concat("\n", Resource1.NoTranslationFoundMessage, "\n"));
            }
            else
            {
                Console.WriteLine(string.Concat("\n", Resource1.TextTranslationMessage));
                Console.WriteLine(translated);
            }
        }

        private static void CommandEnterIteration(PluginTranslatorMap<string> translatorPlugins)
        {
            Console.WriteLine(Resource1.EnterCommandMessage);
            var input = Console.ReadLine();
            var trimmedInput = input.Trim();
            if (trimmedInput.StartsWith("-l "))
            {
                LoadPlugin(trimmedInput, translatorPlugins);
            }
            else if (trimmedInput.StartsWith("-u "))
            {
                UnloadPlugin(trimmedInput, translatorPlugins);
            }
            else if (trimmedInput.StartsWith("-t "))
            {
                Traslate(trimmedInput, translatorPlugins);
            }
            else if (trimmedInput == "-q")
            {
                return;
            }
            else
            {
                Console.WriteLine(string.Concat("\n", Resource1.UnknownCommandMessage));
            }
        }

        private static IPluginTranslatorsContainer CreateInnerContainer(AppDomain appDomain, string pluginPath)
        {
            try
            {
                var runType = typeof(MefTranslatorContainer);
                var runTypeAsmName = runType.Assembly.FullName;
                var wrapRunner = appDomain.CreateInstance(runTypeAsmName, runType.FullName, false,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance,
                    null, new object[] { pluginPath }, null, null);
                var runner = (IPluginTranslatorsContainer)wrapRunner.Unwrap();
                return runner;
            }
            catch (TargetInvocationException ex)
            {
                if (ex.InnerException is PluginException)
                {
                    throw ex.InnerException;
                }

                throw;
            }
        }
        
    }
}
