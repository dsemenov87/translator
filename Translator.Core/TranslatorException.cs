﻿using System;
using System.Runtime.Serialization;

namespace Translator.Core
{
    /// <summary>
    /// Let's design a plug-in such a way they do not raise
    /// the other exceptions.
    /// </summary>
    [Serializable]
    public class TranslatorException : ApplicationException
    {
        public TranslatorException()
        {
        }

        public TranslatorException(string message)
            : base(message)
        {
        }

        public TranslatorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TranslatorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}