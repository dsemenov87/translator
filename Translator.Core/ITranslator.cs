﻿namespace Translator.Core
{
    /// <summary>
    /// Interface to implement plug-ins to display the localization of
    /// the user input to the plugin language.
    /// </summary>
    public interface ITranslator
    {
        string Translate(string inputText);
    }
}