﻿using System;
using System.Runtime.Serialization;

namespace Translator.Plugins.Core
{
    /// <summary>
    /// Let's design a plugin loader such a way they do not raise
    /// the other exceptions.
    /// </summary>
    [Serializable]
    public class PluginException : ApplicationException
    {
        public PluginException()
        {
        }

        public PluginException(string message)
            : base(message)
        {
        }

        public PluginException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected PluginException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}