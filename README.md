# Translator #

### The main scenario is as follows: ###

* the user enters a phrase to be translated and the language code letter for translation;
* the program performs the translation of the phrase word by word with the appropriate module for a given language;
* if the translation is not found - the user is notified;
* translation is carried out by means of plug-ins that implement the following interface:

```csharp
interface ITranslator
{
  string Translate(string userInput); 
}
```
* the loading plug-ins from external assemblies to the console application to display the localization of the user input is implemented.

### Instructions for use ###

* use the `--help` key to get awailable commands;
* run the application in *powershell*;
* set the command-line encoding to *UTF8*: `chcp 65001`.