﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Translator.Plugins.Core;

namespace Translator
{
    public sealed class PluginTranslatorMap<TKey> : IPluginTranslatorMap<TKey>
    {
        public PluginTranslatorMap(IDictionary<TKey, IPluginTranslatorsContainer> containers = null)
        {
            _containers = containers ?? new Dictionary<TKey, IPluginTranslatorsContainer>();
        }
        
        public IPluginTranslatorsContainer this[TKey key]
        {
            get { return _containers[key]; }
            private set { _containers[key] = value; }
        }

        public void Dispose()
        {
            var _ = _containers.Select(i => Remove(i.Key));
        }

        public IEnumerator<KeyValuePair<TKey, IPluginTranslatorsContainer>> GetEnumerator()
        {
            return _containers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IPluginTranslatorMap<TKey> Remove(TKey key)
        {
            if (Contains(key))
            {
                var container =  this[key];
                if (container != null)
                {
                    container.Dispose();
                }
            }
            
            _containers.Remove(key);
            return this;
        }

        public IPluginTranslatorMap<TKey> Add(TKey key, IPluginTranslatorsContainer container)
        {
            _containers[key] = container;
            return this;
        }

        public bool Contains(TKey key)
        {
            return _containers.Keys.Contains(key);
        }

        private readonly IDictionary<TKey, IPluginTranslatorsContainer> _containers;
    }
}
