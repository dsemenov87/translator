﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using Translator.Core;
using Translator.Plugins.Core;

namespace Translator
{
    /// <summary>
    /// The IPluginTranslatorsContainer implementation that uses Managed Extensibility Framework (MEF)
    /// https://msdn.microsoft.com/ru-ru/library/dd460648(v=vs.110).aspx
    /// </summary>
    public class MefTranslatorContainer : MarshalByRefObject, IPluginTranslatorsContainer
    {
        [ImportMany(typeof (ITranslator))]
#pragma warning disable 649
        private IEnumerable<ITranslator> _translators;
#pragma warning restore 649

        public MefTranslatorContainer(string path)
        {
            try
            {
                _catalog = new AggregateCatalog();
                var directoryCatalog = new DirectoryCatalog(path);
                _catalog.Catalogs.Add(directoryCatalog);
                Container = new CompositionContainer(_catalog);
                Container.ComposeParts(this);
            }
            catch (DirectoryNotFoundException ex) { throw PluginError(ex); }
            catch (UnauthorizedAccessException ex) { throw PluginError(ex); }
            catch (PathTooLongException ex) { throw PluginError(ex); }
            catch (ArgumentException ex) { throw PluginError(ex); }
            catch (ObjectDisposedException ex) { throw PluginError(ex); }
            catch (CompositionContractMismatchException ex) { throw PluginError(ex); }
            catch (CompositionException ex) { throw PluginError(ex); }
            catch (NotSupportedException ex) { throw PluginError(ex); }
            catch (TypeLoadException ex) { throw PluginError(ex); }
        }

        public void Dispose()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }

        public string Translate(string inputText, Attribute targetAttribute)
        {
            try
            {
                return _translators
                .Where(t => t.HasAttribute(targetAttribute))
                .Select(t => t.Translate(inputText))
                .FirstOrDefault();
            }
            catch (TypeLoadException ex)
            {
                throw PluginError(ex);
            }
            catch (NotSupportedException ex)
            {
                throw PluginError(ex);
            }
        }

        protected readonly CompositionContainer Container;

        private readonly AggregateCatalog _catalog;
        protected virtual AggregateCatalog Catalog
        {
            get { return _catalog; }
        }

        public PluginException InnerException
        {
            get
            {
                return null;
            }
        }

        private static Exception PluginError(Exception ex)
        {
            return new PluginException(ex.Message);
        }
    }
}