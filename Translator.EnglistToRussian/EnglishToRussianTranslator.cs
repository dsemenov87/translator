﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Translator.Core;

namespace Translator.EnglistToRussian
{
    [Export(typeof(ITranslator))]
    [TranslationLanguage("en-ru")]
    public class EnglishToRussianTranslator : MarshalByRefObject, ITranslator
    {
        private readonly Dictionary<string, string> _dictionary = new Dictionary<string, string>
            {
                {"hello", "привет"},
                {"world", "мир"}
            };

        public string Translate(string input)
        {
            if (input == null)
            {
                throw new TranslatorException("Input text is incorrect.", 
                   new ArgumentNullException("input"));
            }
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            var words = input.Split(' ');
            var translatedWords = words.Select(w =>
            {
                var lowerWord = w.ToLower();
                if (!_dictionary.Keys.Contains(lowerWord))
                {
                    var message = string.Format("Couldn't translate \"{0}\".", w);
                    throw new TranslatorException(message);
                }

                return _dictionary[lowerWord];
            });
            var res = string.Join(" ", translatedWords);
            return res;

        }
    }
}